<?php
namespace App\Helpers;
use DOMDocument;
use DB;
use Mail;
use Auth;
use Session;
use Redirect;




class GlobalFunctions {

	public static function numberFormat($foo){
		return '$ '.number_format((float)$foo, 2, '.', ',');
	}

	public static function dateFormat($foo){
		return date('d/m/y',strtotime($foo));
	}
	public static function dateFormatWithTime($foo){
		return date('d/m/Y H:i:s',strtotime($foo));
	}
	public static function dbdateFormat($oldDate){
		$arr = explode('/', $oldDate);
		return $arr[2].'-'.$arr[1].'-'.$arr[0];
	}
	public static function clean($string) {
		// $string = 'asasa dsawere';
		// $string = str_replace(' ', '_', $string); // Replaces all spaces with hyphens.

		return preg_replace('/[^A-Za-z0-9\-]/', ' ', $string); // Removes special chars.
	}

	public static function createSlug($title, $id = 0)
	{
		$slug = self::clean($title);
		$slug = str_replace(' ', '_', strtolower($slug));
		return $slug;
	}

	public static function generateRandomNo($length){
		//$length = 10;
		$randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
		$randomString1 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
		$randomString = $randomString . $randomString1;
		return $randomString;
	}
	//function to send mail
	public static function sendmail($from,$fromname,$to,$toname,$subject,$data,$mailview,$attachment=null){
		// echo $mailview; die;
			$response =	Mail::send($mailview, $data, function($message) use ($from,$fromname,$to,$toname,$subject,$attachment){
			$message->from($from,$fromname);
			$message->to($to,$toname);
			$message->subject($subject);
			if($attachment != '')
			$message->attach($attachment);
		});

		if(Mail::failures()){
			$response = 0;
		}else
		{
			$response = 1;
		}
		return $response;
	}

	public static function getLatestOrderid()
	{
		//DB::enableQueryLog();
		//$query = DB::getQueryLog();
		$status = 'manual';
		$crypto_payments  = DB::table('payments')->where('payment_type', '!=' , $status)->orderBy('crypto_payorderid', 'desc')->first();
        return $crypto_payments;
	}

}
?>
