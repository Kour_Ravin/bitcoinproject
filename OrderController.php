<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\FromView;
use App\Models\Admin\Product;
use App\Models\Admin\ProductType;
use App\Models\Excel_map_history;
use App\Models\Admin\SgShippingRate;
use App\Models\Order;
use App\Models\OrderAppeal;
use App\Models\OrderItems;
use App\Exports\UsersExport;
use App\Exports\OrderExport;
use App\User;
use App\Product_Price;
use App\Payments;
use DB;
use Auth;
use Helper;
use Log;
use App\Balance;
use App\Services\lib\CryptoboxClass as CryptoboxClassTesT;

use Session;


class OrderController extends Controller
{

    public $url;
    public $countryCode; 
    public $perPage = 10;
    public function __construct()
    {
        $this->url=url(request()->route()->getPrefix());
        $this->prefix='customer/order';
        // $this->middleware(function ($request, $next) {
        //     // fetch session and use it in entire class with constructor
        //     $this->countryCode = session()->get('countryCode');
        //     return $next($request);
        // });
        $this->countryCode = env('APP_COUNTRYCODE');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,$id = null)
    {   
        
        if($request->search == 'empty'){
            $search = '';
        }else{
            $search = $request->search;
        }

        $filter['start_date'] = $start_date = '';
        $filter['end_date'] = $end_date = '';
        $filter['status'] = 'all';

        if(!empty($request->value)||  $search)
        {
            $orders = Order::whereHas('added_by_detail',function($query) use ($search){
            $query->where('users.nick_name','LIKE', '%'.$search.'%');
            })->orwhereHas('supplier_detail',function($query) use ($search){
            $query->where('users.nick_name','LIKE', '%'.$search.'%');
            })
            ->orWhere(function($query) use ($search){
            $query->orWhere('total_item', 'LIKE', '%'.$search.'%');
            $query->orWhere('total_qty', 'LIKE', '%'.$search.'%');
            $query->orWhere('order_number', 'LIKE', '%'.$search.'%');
          })->where('status','!=','7')->orderBy('id','desc')->paginate($request->value);
        }
        else
        {
            $orders = Order::with('supplier_detail','added_by_detail')->whereBetween('created_at', [$start_date." 00:00:00",$end_date." 23:59:59"])->where('status','!=','7')->where('added_by',Auth::user()->id)->orderBy('id','desc')->paginate('10');
        }
        if($request->input('ajax')|| $request->value)
        {
            return view($this->prefix.'/pagination',['title'=>'Orders','filter'=>$filter,'value'=>$request->value,'orders'=>$orders,'url'=>$this->url]);
        }      
        return view($this->prefix.'/index',['title'=>'Orders','filter'=>$filter,'value'=>$request->value,'orders'=>$orders,'url'=>$this->url]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

     /**
     * Excel Field Mapping.
     *
     * @return \Illuminate\Http\Response
     */
    public function excelFieldMapping(Request $request)
    {
        try
        {
            if ($request->isMethod('post')) {    
                $file = $request->file('orderexcelFile');
                $filename = 'order-file-' . time() . '.' . $file->getClientOriginalExtension();
                $destinationPath = base_path().'/assets/orders';
                $file->move($destinationPath,$filename);
                // First sheet
                #Excel::selectSheetsByIndex(0)->load();
                // First and second sheet
                #Excel::selectSheetsByIndex(0, 1)->load();

                $data = \Excel::selectSheetsByIndex(0)->load($destinationPath.'/'.$filename)->get();
           
                $headerRow = $data->first()->keys()->toArray();
                $excelMapHistory = Excel_map_history::where('excel_filed',json_encode($headerRow))->first();

                if(!empty($excelMapHistory)){
                    $mapfieldData = $excelMapHistory->toArray()['map_filed'];
                    $mapfieldData = json_decode($mapfieldData);
                }else{
                    $mapfieldData = array();
                }

                //$excelData = $data->toArray();

                //echo "<prE>"; print_r($headerRow); die;

                return view($this->prefix.'/excelfield-mapping',['title'=>'Order Request','url'=>$this->url,'headerRow' =>$headerRow,'mapfieldData'=>$mapfieldData,'filename'=>$filename]);


            }else{
                return redirect()->back();
            }
        }
        catch (\Exception $e)
        {         
            DB::rollback();
            $errors             = $e->getMessage();
            $res['success']         = false;
            $res['delayTime']       = '5000';
            $res['url']             = '';
            $res['error_message']   = $errors;
            echo json_encode($res);
        }
    }

    /**
     * Confirm Appeal.
     *
     * @return \Illuminate\Http\Response
     */
    public function confirmAppeal(Request $request)
    {
        //echo "<prE>"; print_r($request->all()); die;
        $destinationPath = base_path().'/assets/orders';
        $data = \Excel::selectSheetsByIndex(0)->load($destinationPath.'/'.$request->filename)->get();
        $headerRow = $data->first()->keys()->toArray();
        $excelData = $data->toArray();
        $mapfieldData = array();
        $mapfieldData[] = $request->client_name;
        $mapfieldData[] = $request->client_surname;
        $mapfieldData[] = $request->city;
        $mapfieldData[] = $request->state;
        $mapfieldData[] = $request->zip;
        $mapfieldData[] = $request->country;
        $mapfieldData[] = $request->address;
        $mapfieldData[] = $request->sku;
        $mapfieldData[] = $request->product;
        $mapfieldData[] = $request->quantity;
        $mapfieldData[] = $request->shipping_method;
        $mapfieldData[] = $request->order_id;
        $mapfieldData[] = $request->shipping_from;
        $mapfieldData[] = $request->comments;
        $excelMapHistory = array();
        $lastRowIndex = '';
        $client = $client_surname = $city = $state = $zip = $country = $address1 = $address2 = $shipping_method = $shipping_from = $comments='';
        $excelMapHistory['excel_filed'] = json_encode($headerRow);
        $excelMapHistory['map_filed'] = json_encode($mapfieldData);
        $excelMapHistoryGet = Excel_map_history::where('excel_filed',json_encode($headerRow))->first();
        if(!empty($excelMapHistoryGet)){
            $excelMapHistoryId = $excelMapHistoryGet->toArray()['id'];
        }else{
            $excelMapHistoryId = null;
        }

        $saveMapHistory = Excel_map_history::updateOrCreate(['id' => $excelMapHistoryId], $excelMapHistory);
        $orderArr = array();
        $qtyErrorFlag = 0;
        $productErrorFlag = 0;
        $shippinhMethodErrorFlag = 0;
        $skuStock = array();

       // echo "<prE>"; print_r($mapfieldData); echo "</prE>"; //die;
        //echo "<prE>"; print_r($excelData); echo "</prE>"; die;

        foreach ($excelData as $key => $value) {

            $stockNeeded = 0;
            if($value[$request->order_id] == ''){
                continue;    
            }else{
                if (!is_numeric($value[$request->quantity])) {
                    $qtyErrorFlag = 1;
                    break;                 
                } 
                if ($value[$request->product] == '') {
                    $productErrorFlag = 1;
                    break;                 
                }
                
                if($this->countryCode == 'SG'){
                    $shippingMethod = 'register';
                }else{
                    $shippingMethod = $value[$request->shipping_method];
                }
                
                if (strtolower($shippingMethod) == 'register' || strtolower($shippingMethod) == 'registered' || strtolower($shippingMethod) == 'regular') {
                    $orderArr[$key]['order_id'] = isset($value[$request->order_id])? $value[$request->order_id] : '';
                    $orderArr[$key]['sku'] = isset($value[$request->sku])? $value[$request->sku] : '';
                    $orderArr[$key]['product_id'] = '';
                    $orderArr[$key]['product'] = isset($value[$request->product])? $value[$request->product] : '';
                    $orderArr[$key]['quantity'] = isset($value[$request->quantity]) ? $value[$request->quantity] : '';
                    $orderArr[$key]['shipping_method'] = $shippingMethod;                    
                    $orderArr[$key]['client_name'] = isset($value[$request->client_name]) ? $value[$request->client_name] : '';
                    $orderArr[$key]['client_surname'] = isset($value[$request->client_surname]) ? $value[$request->client_surname] : '';
                    $orderArr[$key]['city'] = isset($value[$request->city]) ? $value[$request->city] : '';
                    $orderArr[$key]['state'] = isset($value[$request->state]) ? $value[$request->state] : '';
                    $orderArr[$key]['zip'] = isset($value[$request->zip]) ? $value[$request->zip] : '';
                    $orderArr[$key]['country'] = isset($value[$request->country]) ? $value[$request->country] : '';
                    $orderArr[$key]['address'] = isset($value[$request->address]) ? $value[$request->address] : '';
                    $orderArr[$key]['shipping_from'] = isset($value[$request->shipping_from]) ? $value[$request->shipping_from] : '';
                    $orderArr[$key]['comments'] = isset($value[$request->comments]) ? $value[$request->comments] : '';
                    //
                    if(isset($value[$request->product])){
                        //$getProductData = Product::select("id","status")->where('product_name',$value[$request->product])->get();
                        $getProductData = Product::select("id","stock","product_type")->where('sku',$value[$request->sku])->get();
                        $getProductData = $getProductData->toArray();
                        if(empty($getProductData)){
                            $orderArr[$key]['status'] = 3;  // Not Applicable
                            $orderArr[$key]['stock'] = 0;
                        }else{
                            if(!isset($skuStock[$value[$request->sku]])){
                                $skuStock[$value[$request->sku]] = 0;
                            }
                            $orderArr[$key]['stock'] = $getProductData[0]['stock']-$skuStock[$value[$request->sku]]; 
                            if(($skuStock[$value[$request->sku]]+$value[$request->quantity]) <= $getProductData[0]['stock']){
                                $orderArr[$key]['status'] = 1; // In Stock  
                                $orderArr[$key]['product_id'] = $getProductData[0]['id']; 
                                $orderArr[$key]['product_type'] = $getProductData[0]['product_type']; 
                                $skuStock[$value[$request->sku]] += $value[$request->quantity];
                            }else{                                
                                $orderArr[$key]['status'] = 2; //Out Of Stock
                            }
                        }
                    }else{
                        $orderArr[$key]['status'] = 3;  // Not Applicable
                        $orderArr[$key]['stock'] = 0;
                    }                    
                }else{
                    $shippinhMethodErrorFlag = 1;
                    break;                 
                }
            }
        }
        
        if($shippinhMethodErrorFlag == 1){
            $res['success']         = false;
            $res['delayTime']       = '5000';
            $res['url']             = '';
            $res['error_message']   = 'Shippling Method should be Register or Regular.';
            echo json_encode($res);
        }else if($productErrorFlag == 1){
            $res['success']         = false;
            $res['delayTime']       = '5000';
            $res['url']             = '';
            $res['error_message']   = 'Product field should not empty';
            echo json_encode($res);
        }else if($qtyErrorFlag == 1){
            $res['success']         = false;
            $res['delayTime']       = '5000';
            $res['url']             = '';
            $res['error_message']   = 'Quantity field should be numeric value';
            echo json_encode($res);
        }else{            
            $confirmOrderHtml =  view($this->prefix.'/confirm-appeal',['title'=>'Order Request','url'=>$this->url,'orderArr' =>$orderArr,'filename'=>$request->filename,'mapfieldData' =>json_encode($mapfieldData)])->render();
            $res['success']         = true;
            //$res['url']             = '';
            $res['delayTime']       = '2000';
            $res['success_message'] = 'Excel field mapped successfully';
            $res['html'] = $confirmOrderHtml;
            echo json_encode($res);    
        }


        
    }

    public function __checkvalue($data,$row,$column)
    {
      if(isset($data[$row][$column]) &&  $data[$row][$column] != ''){
        return $row;
      }
      else {
        return self::__checkvalue($data,$row-1,$column);
      }
    }
    
    /**
     * Place Appeal.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function placeAppeal(Request $request)
    {
        try
        {
            //echo "<prE>"; print_r($request->all()); die;
            DB::beginTransaction();        
            $latestOrderAppeal = OrderAppeal::orderBy('created_at','DESC')->first();
            if(empty($latestOrderAppeal))
            {
                $nextId = 1;
            }
            else
            {
                $nextId = $latestOrderAppeal->id+1;
            }

            $orderAppealData = array();
            $orderAppealData['appeal_number'] = 'REQ'.'-'.substr(strtoupper(auth()->user()->nick_name), 0, 3).str_pad($nextId, 8, "0", STR_PAD_LEFT);
            $orderAppealData['file_name'] = $request->filename;
            $orderAppealData['added_by'] = auth()->user()->id;
            $orderAppealData['total_item'] = count($request->excel_row);
            $orderAppealData['confirm_rows'] = json_encode($request->excel_row);
            //$orderAppealData['deleted_rows'] = json_encode(explode(",",$request->deleterow,-1));
            $orderAppealData['map_field'] = $request->mapfield;                      
            $orderAppealId = null;
            $placeOrderAppeal = OrderAppeal::updateOrCreate(['id' => $orderAppealId], $orderAppealData);
                    
            $skuPriceErrorFlag = array();          
            if(!empty($placeOrderAppeal)){
                $totalAppealQty = 0;
                $excelOrderIds = array();              
                $excelOrderData = array(); 
                $excelClientData = array();  

                if($this->countryCode == 'SG'){
                    #Get SG Customer Shipping Rate
                    $customerRateSg = SgShippingRate::where('user_id',Auth::user()->id)->get()->toArray(); 
                    if(empty($customerRateSg)){
                        DB::rollback();
                        $res['success']         = false;
                        $res['url']             = $this->url.'/order';
                        $res['delayTime']       = '4000';
                        $res['error_message']   = 'Unable to process file because your shipping rates does not updated.Please contact system administrator';  
                        echo json_encode($res);  
                        die();      
                    }
                }else{
                    # Get Customer Shipping Rate
                    $customerRate = User::with('shipping_rate')->where('id',Auth::user()->id)->get(); 
                    if(!empty($customerRate[0]['shipping_rate'])){
                        $regularShippingRate = isset($customerRate[0]['shipping_rate']['regular_shipping_rate'])?$customerRate[0]['shipping_rate']['regular_shipping_rate']:0; 
                        $registerShippingRate = isset($customerRate[0]['shipping_rate']['register_shipping_rate'])?$customerRate[0]['shipping_rate']['register_shipping_rate']:0;     
                    }else{
                        DB::rollback();
                        $res['success']         = false;
                        $res['url']             = $this->url.'/order';
                        $res['delayTime']       = '4000';
                        $res['error_message']   = 'Unable to process file because your shipping rates does not updated.Please contact system administrator';  
                        echo json_encode($res);  
                        die();      
                    } 
                }
                
                // Get all product types               
                $ptArr = ProductType::pluck('product_type')->toArray();
                $ppqArr = ProductType::pluck('per_parcel_qty')->toArray();

                //echo "<prE>"; print_r($ptArr); echo "</prE>";
                //echo "<prE>"; print_r($ppqArr); echo "</prE>"; //die;

                $orderData = array();
                $productTypeQty = array();
                foreach ($request->order_id as $key => $row) {  
                    if($row != '' && $request->status[$key] == 1){                                                
                        # Get Product Price From Database  
                        $productPrice = DB::table('product_prices')
                            ->select('id','price as customerPrice')
                            ->where('product_id', $request->product_id[$key])
                            ->where('user_id',Auth::user()->id)
                            ->get(); 
                        $productPrice = $productPrice->toArray();
                        if(empty($productPrice)){
                            DB::rollback();
                            $res['success']         = false;
                            $res['url']             = $this->url.'/order';
                            $res['delayTime']       = '5000';
                            $res['error_message']   = 'Product price not updated. Please contact system administrator';    
                            echo json_encode($res);  
                            die(); 
                        }     

                        if(isset($productTypeQty[$row][$request->product_type[$key]])){
                            $productTypeQty[$row][$request->product_type[$key]] += $request->quantity[$key];
                        }else{
                            $productTypeQty[$row][$request->product_type[$key]] = $request->quantity[$key];
                        }

                        if (!array_key_exists($row,$orderData)){
                            $orderData[$row] = array();                            
                            $orderData[$row]['appeal_id'] = $placeOrderAppeal->id;
                            $orderData[$row]['order_date'] = date('d-m-Y',strtotime($placeOrderAppeal->created_at));
                            $orderData[$row]['original_id'] = isset($request->order_id[$key])?$request->order_id[$key]:'';
                            $orderData[$row]['added_by'] = auth()->user()->id;
                            $orderData[$row]['client'] = isset($request->client_name[$key])?$request->client_name[$key]:'';
                            $orderData[$row]['client_surname'] = isset($request->client_surname[$key])?$request->client_surname[$key]:'';
                            $orderData[$row]['city'] = isset($request->city[$key])?$request->city[$key]:'';
                            $orderData[$row]['state'] = isset($request->state[$key])?$request->state[$key]:'';
                            $orderData[$row]['zip'] = isset($request->zip[$key])?$request->zip[$key]:'';
                            $orderData[$row]['country'] = isset($request->country[$key])?$request->country[$key]:'';
                            $orderData[$row]['address'] = isset($request->address[$key])?$request->address[$key]:'';
                            $orderData[$row]['shipping_method'] = isset($request->shipping_method[$key])?$request->shipping_method[$key]:'';
                            $orderData[$row]['shipping_from'] = isset($request->shipping_from[$key])?$request->shipping_from[$key]:'';
                            $orderData[$row]['comments'] = isset($request->comments[$key])?$request->comments[$key]:'';
                            
                            $orderData[$row]['total_item'] = 1;                            
                            $orderData[$row]['total_qty'] = isset($request->quantity[$key])?$request->quantity[$key]:'0';
                            
                            /*if(ucfirst($orderData[$row]['shipping_method']) == 'Regular'){
                                $orderData[$row]['customer_shipping'] = $regularShippingRate;
                            }else{
                                $orderData[$row]['customer_shipping'] = $registerShippingRate;
                            }*/ 
                         
                            $orderData[$row]['customer_amt'] = $productPrice[0]->customerPrice*$request->quantity[$key];
                            //$orderData[$row]['total_customer'] = ($orderData[$row]['customer_amt']+$orderData[$row]['customer_shipping']);
                            $orderData[$row]['total_customer'] = $orderData[$row]['customer_amt'];

                            $itemData = array();
                            $itemData['sku'] = $request->sku[$key];
                            $itemData['product_id'] = $request->product_id[$key];
                            $itemData['product'] = $request->product[$key];
                            $itemData['quantity'] = $request->quantity[$key];
                            $itemData['stock'] = $request->stock[$key];
                            $itemData['new_stock'] = ($request->stock[$key]-$request->quantity[$key]); 
                            $itemData['customer_price'] = $productPrice[0]->customerPrice;                         
                            $totalAppealQty += $request->quantity[$key]; 

                            $orderData[$row]['itemData'][$key] = $itemData;

                        }else{   
                            $orderData[$row]['total_item'] += 1;
                            $orderData[$row]['total_qty'] += isset($request->quantity[$key])?$request->quantity[$key]:'0';
                            $orderData[$row]['customer_amt'] += ($productPrice[0]->customerPrice*$request->quantity[$key]);
                            $orderData[$row]['total_customer'] = $orderData[$row]['customer_amt'];

                            $itemData = array();
                            $itemData['sku'] = $request->sku[$key];
                            $itemData['product_id'] = $request->product_id[$key];
                            $itemData['product'] = $request->product[$key];
                            $itemData['quantity'] = $request->quantity[$key];
                            $itemData['stock'] = $request->stock[$key];
                            $itemData['new_stock'] = ($request->stock[$key]-$request->quantity[$key]); 
                            $itemData['customer_price'] = $productPrice[0]->customerPrice;                         
                            $totalAppealQty += $request->quantity[$key]; 

                            $orderData[$row]['itemData'][$key] = $itemData;
                        }
                    }                                      
                }

                //echo $this->countryCode."<pre>"; print_r($orderData); die;
                //echo "<pre>"; print_r($productTypeQty); die;

                $orderCount = 0;  
                $appealAmount = 0;  
                $appealShipping = 0;  
                foreach ($orderData as $key => $order) {
                    //echo "<pre>"; print_r($order); die;
                    #Get Order Last Id
                    $latestOrder = Order::orderBy('id','DESC')->first();
                    if(empty($latestOrder))
                    {
                        $nextId = 1;
                    }
                    else
                    {
                        $nextId = $latestOrder->id+1;
                    }

                    $insertOrder = array();
                    $insertOrder['appeal_id'] = $order['appeal_id'];
                    $insertOrder['order_date'] = $order['order_date'];
                    $insertOrder['order_number'] = substr(strtoupper(auth()->user()->nick_name), 0, 3).str_pad($nextId, 8, "0", STR_PAD_LEFT);
                    $insertOrder['original_id'] = $order['original_id'];
                    $insertOrder['added_by'] = $order['added_by'];
                    $insertOrder['client'] = $order['client'];
                    $insertOrder['client_surname'] = $order['client_surname'];
                    $insertOrder['city'] = $order['city'];
                    $insertOrder['state'] = $order['state'];
                    $insertOrder['zip'] = $order['zip'];
                    $insertOrder['country'] = $order['country'];
                    $insertOrder['address'] = $order['address'];
                    $insertOrder['shipping_method'] = $order['shipping_method'];
                    $insertOrder['shipping_from'] = $order['shipping_from'];
                    $insertOrder['comments'] = $order['comments'];
                    $insertOrder['total_item'] = $order['total_item'];
                    $insertOrder['total_qty'] = $order['total_qty'];
                    $insertOrder['customer_amt'] = $order['customer_amt'];
                    //$insertOrder['customer_shipping'] = $order['customer_shipping'];
                    //$insertOrder['total_customer'] = $order['total_customer'];

                    $shippingCharge = 0;
                    if($this->countryCode == 'SG'){
                        /*if($order['total_qty'] < 101 ){
                            $shippingCharge = 11;
                        }else if($order['total_qty'] > 100 && $order['total_qty'] < 301 ){
                            $shippingCharge = 13;
                        }else if($order['total_qty'] > 300 && $order['total_qty'] < 501 ){
                            $shippingCharge = 15;
                        }else if($order['total_qty'] > 500 && $order['total_qty'] < 701 ){
                            $shippingCharge = 18;
                        }else{
                            $shippingCharge = 18;        
                        }*/

                        foreach ($customerRateSg as $key => $shippRate) {
                            $range = explode('-', $shippRate['pills_range']);
                            if($order['total_qty'] > $range[0] && $order['total_qty'] <= $range[1]){
                                $shippingCharge = $shippRate['shipping_rate'];
                            }
                            $lastShippingRate =  $shippRate['shipping_rate'];        
                        }
                        if($shippingCharge == 0){
                            $shippingCharge = $lastShippingRate;
                        }
                        $insertOrder['customer_shipping'] = $shippingCharge;
                        $insertOrder['total_customer'] = $order['total_customer']+$insertOrder['customer_shipping'];
                    }else{
                        if(strtolower($order['shipping_method']) == 'regular'){
                            $insertOrder['customer_shipping'] = $shippingCharge = $regularShippingRate;
                            //$insertOrder['total_customer'] = $order['total_customer']+$insertOrder['customer_shipping'];
                        }else{
                            $insertOrder['customer_shipping'] = $shippingCharge = $registerShippingRate;
                            //$insertOrder['total_customer'] = $order['total_customer']+$insertOrder['customer_shipping'];    
                        }                        
                    }

                    # Calculate number of parsal
                    $noOfParcel = 1;
                    if($this->countryCode != 'SG'){                        
                        /*if($order['total_qty'] > env('STORE_PER_PARCEL_ITEM')){
                            $noOfParcel = ceil($order['total_qty']/env('STORE_PER_PARCEL_ITEM'));                            
                        }*/
                        $thisParcelQty = 0;
                        foreach ($productTypeQty[$order['original_id']] as $key => $ptq) {
                            //echo "<prE>"; print_r($order); die;
                            $thisParcelQty +=  $ptq/$ppqArr[$key-1];
                        }  
                        $noOfParcel = ceil($thisParcelQty);                     
                    }

                    if($shippingCharge == 0){
                        $finalCustomerAmt = $order['customer_amt'];
                    }else{
                        $finalCustomerAmt = $order['customer_amt']+($shippingCharge*$noOfParcel);    
                    }
                    
                    $insertOrder['no_of_parcel'] = $noOfParcel;    
                    $insertOrder['total_customer'] = $finalCustomerAmt;    

                    //echo "<prE>"; print_r($insertOrder); die;
                    $appealAmount += $insertOrder['customer_amt'];
                    $appealShipping += $insertOrder['customer_shipping'];
                    #Insert Order
                    $saveOrder = Order::updateOrCreate(['id' => null], $insertOrder);
                    $orderCount++;
                    #Inser Order Items
                    foreach ($order['itemData'] as $key => $itemData) {
                        //echo "<prE>"; print_r($itemData); die;
                        $insertOrderItem['order_id'] = $saveOrder->id;
                        $insertOrderItem['sku'] = $itemData['sku'];
                        $insertOrderItem['product'] = $itemData['product'];
                        $insertOrderItem['quantity'] = $itemData['quantity'];
                        $insertOrderItem['customer_price'] = $itemData['customer_price'];
                        $saveOrderItem = OrderItems::updateOrCreate(['id' => null], $insertOrderItem);

                        #Update Stock
                        $productData = array();
                        $productData['stock'] = $itemData['new_stock'];
                        $updateProduct = Product::updateOrCreate(['id' => $itemData['product_id']], $productData); 
                    }                    
                }

                #Update Apeal Details 
                $orderAppealUpdateData = array();
                $orderAppealUpdateData['total_qty'] = $totalAppealQty;
                $orderAppealUpdateData['amount'] = $appealAmount;
                $orderAppealUpdateData['shipping'] = $appealShipping;
                $orderAppealUpdateData['total_amount'] = ($appealAmount+$appealShipping);

                $orderAppealId = $placeOrderAppeal->id;
                $placeOrderAppeal = OrderAppeal::updateOrCreate(['id' => $orderAppealId], $orderAppealUpdateData);
                    
                $orderBy = Auth::user()->first_name.' '.Auth::user()->last_name;
                $maildata = array('requestNumber'=>$orderAppealData['appeal_number'],'customerName'=>$orderBy,'totalOrder'=>$orderCount,'url'=>url('/'));
                # Order Request Mail Notification
                $dropshipperAdmin = User::where('id',1)->get();
                $adminName = $dropshipperAdmin[0]['first_name'].' '.$dropshipperAdmin[0]['last_name'];
                $adminEmail = $dropshipperAdmin[0]['email'];                   
                # Order Request Mail Notification
                $fromEmail = env('MAIL_FROM');
                $fromName = env('MAIL_FROMNAME');
                $mailresult = Helper::sendmail($fromEmail,$fromName, $adminEmail, $adminName, 'New order request', $maildata,'mail.adminOrderRequestNotification');
                
                if($mailresult){
                    DB::commit();
                    $orderAppealId = $placeOrderAppeal->toArray()['id'];
                    $res['success']         = true;
                    $res['url']             = $this->url.'/order';
                    $res['delayTime']       = '2000';
                    $res['success_message'] = 'Order Request Placed successfully';
                    echo json_encode($res);  
                    die();      
                }                                         
            }    
        }
        catch (\Exception $e)
        {         
            DB::rollback();
            $errors             = $e->getMessage();
            $res['success']         = false;
            $res['delayTime']       = '5000';
            $res['url']             = '';
            $res['error_message']   = $errors;
            echo json_encode($res);
        }
    }

     /**
     * Show order in detail.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function detailOrder(Request $request)
    {
        try
        {
            $orderId = \Crypt::decrypt($request->orderId);
            //$orderItems = OrderItems::with('order_detail')->where('order_id',$orderId)->get();
            $orderItems = Order::with('items_detail')->where('id',$orderId)->get();
            $orderItems = $orderItems->toArray();
            //echo "<prE>"; print_r($orderItems); echo "</prE>"; die;
            $orderItemsHtml = view($this->prefix.'/order-detail',['url'=>$this->url,'orderItems' => $orderItems[0],'orderId'=>$orderId])->render();
            $res['success']         = true;
            //$res['url']             = '';
            $res['delayTime']       = '2000';
            //$res['success_message'] = 'Excel field mapped successfully';
            $res['html'] = $orderItemsHtml;
            echo json_encode($res);
        }
        catch (\Exception $e)
        {
            DB::rollback();
            $errors             = $e->getMessage();
            $res['success']     = false;
            $res['formErrors']  = true;
            $res['errors']      = $errors;
            echo json_encode($res);
            die();
        }
    }

    /**
     * Download order in detail.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function downloadOrder(Request $request)
    {  
        try
        {            
            $filter = \Crypt::decrypt($request->id); 
            $filter = explode('/', $filter);
            
            //echo "<prE>"; print_r($filter); //die;

            $start_date = $filter[0];
            $end_date = $filter[1];
            $status = $filter[2];

            if($status == 'unprocessed'){                       
                $orderStatus = '0';
            }else if($status == 'processed'){  
                $orderStatus = '1';
            }else if($status == 'reshipping'){
                $orderStatus = '2';
            }else if($status == 'shipped'){  
                $orderStatus = '5'; // change 4 to 5 after auto payment
            }else if($status == 'canceled'){
                $orderStatus = '6';
            }else{
                $orderStatus = 'all';
            }

            $orderItems = DB::table('order_items')
                ->leftJoin('orders', 'orders.id', '=', 'order_items.order_id')
                ->select('order_items.id as Item_ID','orders.order_number as Order_Number','orders.original_id as Order_Id',DB::raw("CONCAT(orders.client,' ',orders.client_surname)  AS Full_Name"),'orders.address as Address','orders.city as City','orders.state as State','orders.zip as Zip','orders.country as Country','order_items.sku as SKU','order_items.product as Product','order_items.quantity as Quantity','orders.shipping_method as Shipping_Method','orders.tracking_number as Tracking_Number','order_items.send_status as Send_Status',DB::raw('(order_items.quantity * order_items.customer_price) as Total_Amount'),'orders.comments as Comments')
                ->where('orders.added_by',Auth::user()->id)
                ->Where(function($query) use($start_date,$end_date,$status,$orderStatus)
                {
                    if($status != 'all'){
                        if($status == 'canceled'){
                            $query->where('status','>','5');  // Cancel or redirect both
                        }else{
                            $query->where('status',$orderStatus);  
                        }    
                    } 
                    if($start_date != '' && $end_date != ''){
                        $query->whereBetween('orders.created_at', [$start_date." 00:00:00",$end_date." 23:59:59"]);
                    }                  
                })->get();

            //echo "<prE>"; print_r($orderItems); die;

            //if(!empty($orderItems)){

                $filename = "Orders";
                $data = $orderItems->toArray();
                $data= json_decode( json_encode($data), true);

                return Excel::create($filename, function($excel) use ($data) {
                 
                    $excel->sheet('Orders', function($sheet) use ($data) {                        
                        $sheet->setColumnFormat(array(
                            "P" => '0.00',
                        ));
                        $sheet->loadView($this->prefix.'/export-orders')->with('orderItems',$data);
                        $sheet->setOrientation('landscape');
                    });
                })->download('xlsx');
            // }else{               
            //     $res['success']     = false;
            //     $res['formErrors']  = true;
            //     $res['errors']      = "No items found for download.";
            //     echo json_encode($res);
            //     die();    
            // }  
        }
        catch (\Exception $e)
        {
            DB::rollback();
            $errors             = $e->getMessage();
            $res['success']     = false;
            $res['formErrors']  = true;
            $res['errors']      = $errors;
            echo json_encode($res);
            die();
        }
    }

    public function downloadStatment(Request $request)
    {  
        try
        {            
            $filter = \Crypt::decrypt($request->id); 
            $filter = explode('/', $filter);
            
            $start_date = $filter[0];
            $end_date = $filter[1];
            $status = $filter[2];

            if($status == 'unprocessed'){                       
                $orderStatus = '0';
            }else if($status == 'processed'){  
                $orderStatus = '1';
            }else if($status == 'reshipping'){
                $orderStatus = '2';
            }else if($status == 'shipped'){  
                $orderStatus = '5'; // change 4 to 5 after auto payment
            }else if($status == 'canceled'){
                $orderStatus = '6';
            }else{
                $orderStatus = 'all';
            }

            $orders =  Order::select('id','order_number as Order_Number','original_id as Original_Id','order_date as Order_Date','client as Client','city as City','state as State','zip as Zip','country as Country','address as Address','total_item As Items','total_qty As Quantity','no_of_parcel As No_Of_Parcel','customer_amt as Order_Amount','customer_shipping as Per_Parcel_Shipping','total_customer as Total_Amount','tracking_number as Tracking_Number')
            ->where('added_by',Auth::user()->id)
            ->Where(function($query) use($start_date,$end_date,$status,$orderStatus)
            {                

                if($status != 'all'){
                    if($status == 'canceled'){
                        $query->where('status','>','5');  // Cancel or redirect both
                    }else{
                        $query->where('status',$orderStatus);  
                    }    
                }else{
                     $query->where('status','<','6');  // No Cancel or redirect
                } 
                if($start_date != '' && $end_date != ''){
                    $query->whereBetween('orders.created_at', [$start_date." 00:00:00",$end_date." 23:59:59"]);
                }                  
            })->with('items_detail')->get()->toArray();   
            //echo count($orders); echo "<prE>"; print_r($orders); die; 
            
            $user_id = Auth::user()->id;
            $orderItems = OrderItems::with('order_detail')->whereHas('order_detail',function($query) use ($start_date,$end_date,$status,$orderStatus,$user_id){
                $query->where('added_by',$user_id);
                if($status != 'all'){
                    if($status == 'canceled'){
                        $query->where('status','>','5');  // Cancel or redirect both
                    }else{
                        $query->where('status',$orderStatus);  
                    }    
                }else{
                     $query->where('status','<','6');  // No Cancel or redirect
                } 
                if($start_date != '' && $end_date != ''){
                    $query->whereBetween('orders.created_at', [$start_date." 00:00:00",$end_date." 23:59:59"]);
                } 
            })->get()->toArray();
            
            //echo "<prE>"; print_r($orderItems); die;

            /*$orders =  Order::select('order_number as Order_Number','original_id as Original_Id','order_date as Order_Date','client as Client','city as City','state as State','zip as Zip','country as Country','address as Address','total_item As Items','total_qty As Quantity','no_of_parcel As No_Of_Parcel','customer_amt as Order_Amount','customer_shipping as Per_Parcel_Shipping','total_customer as Total_Amount','tracking_number as Tracking_Number')
            ->where('added_by',Auth::user()->id)
            ->Where(function($query) use($start_date,$end_date,$status,$orderStatus)
            {
                if($status != 'all'){
                    if($status == 'canceled'){
                        $query->where('status','>',5);  // Cancel or redirect both
                    }else{
                        $query->where('status',$orderStatus);  
                    }    
                } 
                if($start_date != '' && $end_date != ''){
                    $query->whereBetween('orders.created_at', [$start_date." 00:00:00",$end_date." 23:59:59"]);
                }                  
            })->get()->toArray(); */

            //echo "<prE>"; print_r($orders); die; 
            $payments = DB::table('payments')
                ->leftJoin('users', 'users.id', '=', 'payments.user_id')
                ->select(DB::raw('DATE(payments.created_at) as Date'),'payments.Amount','payments.Notes',DB::raw('CONCAT(first_name," ",last_name) AS full_name'))
               ->where('user_id',Auth::user()->id)->get()->toArray(); 
        
            
            //if(!empty($orders)){
                $filename = "Orders Statment";                
                $countOrders = count($orders);
                $countPayments = count($payments);
                $paymentStyleStart = ($countOrders+9); 
                $paymentStyleEnd = ($paymentStyleStart+$countPayments);
                //echo "B$paymentStyleStart:B$paymentStyleEnd"; die;
                return Excel::create($filename, function($excel) use ($orders,$orderItems,$payments,$paymentStyleStart,$paymentStyleEnd) {
                 
                    $excel->sheet('Statement', function($sheet) use ($orders,$payments,$paymentStyleStart,$paymentStyleEnd) {
                        $sheet->setColumnFormat(array(
                            "C$paymentStyleStart:C$paymentStyleEnd" => '0.00',
                            //"J" => '0.00',
                            //"K" => '0.00',
                            //"L" => '0.00',
                            "M" => '0.00',
                            "N" => '0.00',
                            "O" => '0.00',
                        ));
                        $sheet->loadView($this->prefix.'/export-statments')->with('orders',$orders)->with('payments',$payments);
                        $sheet->setOrientation('landscape');                    
                    });
                    $excel->sheet('Statment Items', function($sheet) use ($orders) {
                        $sheet->setColumnFormat(array(                            
                            "H" => '0.00',
                            //"J" => '0.00',
                            //"K" => '0.00',                             
                        ));
                        $sheet->loadView($this->prefix.'/export-statment-items')->with('orders',$orders);
                        $sheet->setOrientation('landscape');                    
                    });
                })->download('xlsx');
            // }else{               
            //     $res['success']     = false;
            //     $res['formErrors']  = true;
            //     $res['errors']      = "No items found for download.";
            //     echo json_encode($res);
            //     die();    
            // }
        }
        catch (\Exception $e)
        {
            DB::rollback();
            $errors             = $e->getMessage();
            $res['success']     = false;
            $res['formErrors']  = true;
            $res['errors']      = $errors;
            echo json_encode($res);
            die();
        }
    }

    /**
     * Export user example.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function exportOrder(Request $request)
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request)
        {     
            try
            {
                $orderId = \Crypt::decrypt($request->orderId);
                //$orderItems = OrderItems::with('order_detail')->where('order_id',$orderId)->get();
                $orderItems = Order::with('items_detail')->where('id',$orderId)->get();
                $orderItems = $orderItems->toArray();              
                //echo "<prE>"; print_r($orderItems); echo "</prE>"; die;
                $orderItemsHtml = view($this->prefix.'/edit-orders',['url'=>$this->url,'orderItems' => $orderItems[0],'orderId'=>$orderId])->render();                
                $res['success']         = true;
                //$res['url']             = '';
                $res['delayTime']       = '5000';
                //$res['success_message'] = 'Excel field mapped successfully';
                $res['html'] = $orderItemsHtml;
                echo json_encode($res);
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $errors             = $e->getMessage();
                $res['success']     = false;
                $res['formErrors']  = true;
                $res['errors']      = $errors;
                echo json_encode($res);
                die();
            }
        }

        public function updateorder(Request $request){
            try
            {
                DB::beginTransaction();
                //form serialized data
                $orderid = \Crypt::decrypt($request->client_order_id);
                $order_status = $request->order_status;
                $client_id = $request->client_id;
                $supplier_id = $request->supplier_id;
                $order_sku  =$request->edit_order_sku;
                $order_quantity  =$request->edit_order_quantity;
                $order_quantity_old  =$request->old_order_quantity;
                $order_shipping  =$request->order_shipping;//old_shipping amount
                $order_sup_shipping  =$request->order_sup_shipping; //old_shipping amount
                $order_amt_old  =$request->customer_order_amt;//Old shipping amt
                $appeal_id  = $request->appeal_id;
                $order_item_id = $request->order_item_id;
                $updateEditedClientData['client'] = $request->client_name ;
                $updateEditedClientData['client_surname'] = $request->client_surname ; 
                $updateEditedClientData['city'] =$request->client_city ;
                $updateEditedClientData['state'] = $request->client_state ;
                $updateEditedClientData['zip'] =  $request->client_zc;
                $updateEditedClientData['country'] =$request->client_country ;
                $updateEditedClientData['address'] =$request->client_address ;
                
                $updateOrder = Order::updateOrCreate(['id' => $orderid], $updateEditedClientData);
                $updatedPrice = 0;
                $supUpdatedPrice = 0;
                $updated_order_quantity = 0;
                $old_order_quantity = 0;
                $productTypeQty = array();
                $productSKUQty = array();
                
                foreach ($order_sku as $key => $item) { 
                    $productPrice = DB::table('products')
                    ->crossJoin('product_prices', 'product_prices.product_id', '=', 'products.id')
                    ->select('product_prices.price as productPrice','products.product_type as productType','products.stock as Stock')
                    ->where('products.sku', $item)
                    ->where('product_prices.user_id',$client_id)
                    ->get();
                    if($productPrice[0]){   
                        
                        if(isset($productTypeQty[$productPrice[0]->productType])){
                            $productTypeQty[$productPrice[0]->productType] += $order_quantity[$key];
                        }else{
                            $productTypeQty[$productPrice[0]->productType] = $order_quantity[$key];
                        }

                        $old_order_quantity +=  $order_quantity_old[$key];
                        $updateEditedOrderData = array();
                        $updatedPrice += ($productPrice[0]->productPrice*$order_quantity[$key]);
                        $updateEditedOrderData['quantity'] = $order_quantity[$key];
                        $OrderUpdated = OrderItems::updateOrCreate(['id' => $order_item_id[$key]], $updateEditedOrderData);
                        $updated_order_quantity +=  $order_quantity[$key];
                    
                        // SKU Stock Update
                        $newStock = $productPrice[0]->Stock+($order_quantity[$key]-$order_quantity_old[$key]);
                        $stockData = array();
                        $stockData['stock'] = $newStock;
                        $updateStock = Product::updateOrCreate(['sku' => $item], $stockData);

                    }else{
                        DB::rollback();
                        $res['success']         = false;
                        $res['delayTime']       = '3000';
                        $res['url']             = $this->url.'/order';
                        $res['error_message']   = 'Data not updated for this order.';
                        echo json_encode($res);
                        die(); 
                    } 

                    // If order assigned to supplier the change supplier price also
                    // Processed Order edit supplier price
                    if($order_status > 0){
                        $supplierProductPrice = DB::table('products')
                        ->crossJoin('product_prices', 'product_prices.product_id', '=', 'products.id')
                        ->select('product_prices.price as productPrice','products.product_type as productType','products.stock as Stock')
                        ->where('products.sku', $item)
                        ->where('product_prices.user_id',$supplier_id)
                        ->get()->toArray();    
                    }

                    if(!empty($supplierProductPrice)){   
                       $supUpdatedPrice += ($supplierProductPrice[0]->productPrice*$order_quantity[$key]);
                    }else{
                        DB::rollback();
                        $res['success']         = false;
                        $res['delayTime']       = '3000';
                        $res['url']             = $this->url.'/order';
                        $res['error_message']   = 'Supplier product price not updated. Please contact system administrator';
                        echo json_encode($res);
                        die(); 
                    } 

                }

                // Get all product types               
                $ptArr = ProductType::pluck('product_type')->toArray();
                $ppqArr = ProductType::pluck('per_parcel_qty')->toArray();
           
                #Calculate number of parsal
                $noOfParcel = 1;
                if($this->countryCode != 'SG'){                        
                    /*if($order['total_qty'] > env('STORE_PER_PARCEL_ITEM')){
                        $noOfParcel = ceil($order['total_qty']/env('STORE_PER_PARCEL_ITEM'));                            
                    }*/
                    $thisParcelQty = 0;
                    foreach ($productTypeQty as $key => $ptq) {
                        //echo "<prE>"; print_r($order); die;
                        $thisParcelQty +=  $ptq/$ppqArr[$key-1];
                    }  
                    $noOfParcel = ceil($thisParcelQty);                     
                }

                // Order-tab  Updations
                $updateOrderTab['total_qty']= $updated_order_quantity;
                $updateOrderTab['no_of_parcel']= $noOfParcel;
                $updateOrderTab['customer_amt']= $updatedPrice;
                $updateOrderTab['total_customer']= ($order_shipping*$noOfParcel)+$updatedPrice;
                $updateOrderTab['seller_amt']= $supUpdatedPrice;
                $updateOrderTab['total_seller']= ($order_sup_shipping*$noOfParcel)+$supUpdatedPrice;
                $OrderUpdated = Order::updateOrCreate(['id' => $orderid],$updateOrderTab);

                //Updation of Appeal Tab
                $appealOldData = OrderAppeal::select('total_qty','amount','total_amount')->where('id',$appeal_id)->get()->toArray();
                
                $calculated_quantity = ($appealOldData[0]['total_qty'] - $old_order_quantity) + $updated_order_quantity;
                $calculated_amount = ($appealOldData[0]['amount'] - $order_amt_old) + $updatedPrice;
                $calculated_total_amount = ($appealOldData[0]['total_amount'] - $order_amt_old) + $updatedPrice;

                $updateOrderAppealTab['total_qty']=$calculated_quantity;
                $updateOrderAppealTab['amount']=$calculated_amount;
                $updateOrderAppealTab['total_amount']=$calculated_total_amount;
                
                $orderAppealUpdate = OrderAppeal::updateOrCreate(['id' => $appeal_id],$updateOrderAppealTab);

                DB::commit();              
                $res['success']         = true;
                $res['url']             = $this->url.'/order';
                $res['delayTime']       = 2000;
                $res['success_message'] = 'Order Updated Successfully';
                echo json_encode($res);
                die(); 

            }
            
            catch (\Exception $e)
            {
                DB::rollback();
                $errors             = $e->getMessage();
                $res['success']     = false;
                $res['formErrors']  = true;
                $res['errors']      = $errors;
                $res['delayTime']       = '5000';
                echo json_encode($res);
                die();
            }
        }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function searchadmin_order_old(Request $request){
        if($request->search == 'empty'){
            $search = '';
          }else{
            $search = $request->search;
          }
           if($request->limit == 'empty'){
            $limit = '';
          }else{
            $limit = $request->limit;
          }


          $orders = Order::whereHas('added_by_detail',function($query) use ($search){
            $query->  where('users.nick_name','LIKE', '%'.$search.'%');
           })->orwhereHas('supplier_detail',function($query) use ($search){
            $query->  where('users.nick_name','LIKE', '%'.$search.'%');
           })
            ->orWhere(function($query) use ($search){
            $query->orWhere('total_item', 'LIKE', '%'.$search.'%');
            $query->orWhere('total_qty', 'LIKE', '%'.$search.'%');
            $query->orWhere('order_number', 'LIKE', '%'.$search.'%');
          })
            ->orderBy('id','desc')->paginate($request->limit);

        //$query = DB::getQueryLog();
        $orderCOUNT = count($orders);
        //return view($this->prefix.'/pagination',['patients'=>$patients,'patientCount'=>$patientCount]);
        return view($this->prefix.'/pagination',['orders'=>$orders,'OrderCount'=>$orderCOUNT,'url'=>$this->url]);
    }

    public function searchadmin_order(Request $request){

        // Except pagination reset current pae
        if(!isset($request['ajax'])){
            $currentPage = 1; 
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });    
        }

        $filter = array();
        
        if(request()->segment(3) == 'empty'){
            $filter['search'] =  $search = '';
        }else{
            $filter['search'] =  $search = request()->segment(3);
        }
        if(request()->segment(4) != 'empty'){
            $this->perPage = request()->segment(4); 
        } 
        if(request()->segment(5) == 'empty'){
            $filter['start_date'] =  $start_date = '';
        }else{
            $filter['start_date'] =  $start_date = request()->segment(5);
        }        
        if(request()->segment(6) == 'empty'){
            $filter['end_date'] =  $end_date = '';
        }else{
            $filter['end_date'] =  $end_date = request()->segment(6);
        }
        
        $filter['status'] = $status = request()->segment(7);
       
        if($status == 'unprocessed'){                       
            $orderStatus = '0';
        }else if($status == 'processed'){  
            $orderStatus = '1';
        }else if($status == 'reshipping'){
            $orderStatus = '2';
        }else if($status == 'shipped'){  
            $orderStatus = '5'; // change 4 to 5 after auto payment
        }else if($status == 'canceled'){
            $orderStatus = '6';
        }else{
            $orderStatus = 'all';
        }

        //echo "<prE>"; print_r($filter);  echo "</prE>"; //die;

        if($filter['search'] != ''){  
            //DB::enableQueryLog();
            if($filter['start_date'] != '' && $filter['start_date'] != ''){   
                $orders = Order::whereHas('added_by_detail',function($query) use ($search){
                $query->  where('users.nick_name','LIKE', '%'.$search.'%');
                })->orwhereHas('supplier_detail',function($query) use ($search){
                    $query->  where('users.nick_name','LIKE', '%'.$search.'%');
                })
                ->orWhere(function($query) use ($search,$orderStatus,$start_date,$end_date){
                    $query->orWhere('total_item', 'LIKE', '%'.$search.'%');
                    $query->orWhere('total_qty', 'LIKE', '%'.$search.'%');
                    $query->orWhere('order_number', 'LIKE', '%'.$search.'%');
                    $query->orWhere('original_id', 'LIKE', '%'.$search.'%');
                    $query->whereBetween('created_at', [$start_date." 00:00:00",$end_date." 23:59:59"]);
                    if($orderStatus != 'all'){
                        $query->where('status',$orderStatus);   
                    }else{
                        $query->where('status','!=','7'); // Redirected order will not show to customer
                    }
                })
                ->where('added_by',Auth::user()->id)->orderBy('id','desc')->paginate($this->perPage); 

            }else{
                $orders = Order::whereHas('added_by_detail',function($query) use ($search){
                $query->  where('users.nick_name','LIKE', '%'.$search.'%');
                })->orwhereHas('supplier_detail',function($query) use ($search){
                    $query->  where('users.nick_name','LIKE', '%'.$search.'%');
                })
                ->orWhere(function($query) use ($search,$orderStatus){
                    $query->orWhere('total_item', 'LIKE', '%'.$search.'%');
                    $query->orWhere('total_qty', 'LIKE', '%'.$search.'%');
                    $query->orWhere('order_number', 'LIKE', '%'.$search.'%');
                    $query->orWhere('original_id', 'LIKE', '%'.$search.'%');
                    if($orderStatus != 'all'){
                        $query->where('status',$orderStatus);   
                    }else{
                        $query->where('status','!=','7'); // Redirected order will not show to customer
                    }
                })->where('added_by',Auth::user()->id)->orderBy('id','desc')->paginate($this->perPage);                 
            }
            //DB::enableQueryLog();            
        }else{   

            if($filter['start_date'] != '' && $filter['start_date'] != ''){               
                if($orderStatus != 'all'){
                    $orders = Order::with('supplier_detail','added_by_detail')->whereBetween('created_at', [$start_date." 00:00:00",$end_date." 23:59:59"])->where('status',$orderStatus)->where('added_by',Auth::user()->id)->orderBy('id','desc')->paginate($this->perPage);   
                }else{
                    $orders = Order::with('supplier_detail','added_by_detail')->whereBetween('created_at', [$start_date." 00:00:00",$end_date." 23:59:59"])->where('status','!=','7')->where('added_by',Auth::user()->id)->orderBy('id','desc')->paginate($this->perPage);    
                }

            }else{
                if($orderStatus != 'all'){
                    $orders = Order::with('supplier_detail','added_by_detail')->where('status',$orderStatus)->where('added_by',Auth::user()->id)->orderBy('id','desc')->paginate($this->perPage);   
                }else{
                    $orders = Order::with('supplier_detail','added_by_detail')->where('added_by',Auth::user()->id)->orderBy('id','desc')->paginate($this->perPage);    
                }                                          
            } 
        }
        $suppliers = User::whereHas('roles', function ($query) { $query->where('name', '=', 'supplier'); })->where('status','1')->orderBy('id','desc');
        $orderCOUNT = count($orders);
        return view($this->prefix.'/pagination',['filter'=> $filter,'value'=>$this->perPage,'orders'=>$orders,'OrderCount'=>$orderCOUNT,'url'=>$this->url]);
    }

    public function searchadmin_accounting(Request $request){

        // Except pagination reset current pae
        if(!isset($request['ajax'])){
            $currentPage = 1; 
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });    
        }

        $filter = array();
        
        if(request()->segment(3) == 'empty'){
            $filter['search'] =  $search = '';
        }else{
            $filter['search'] =  $search = request()->segment(3);
        }
        if(request()->segment(4) != 'empty'){
            $this->perPage = request()->segment(4); 
        } 
        if(request()->segment(5) == 'empty'){
            $filter['start_date'] =  $start_date = '';
        }else{
            $filter['start_date'] =  $start_date = request()->segment(5);
        }        
        if(request()->segment(6) == 'empty'){
            $filter['end_date'] =  $end_date = '';
        }else{
            $filter['end_date'] =  $end_date = request()->segment(6);
        }
        
        $filter['status'] = $status = request()->segment(7);
       
        if($status == 'unprocessed'){                       
            $orderStatus = '0';
        }else if($status == 'processed'){  
            $orderStatus = '1';
        }else if($status == 'reshipping'){
            $orderStatus = '2';
        }else if($status == 'shipped'){  
            $orderStatus = '5'; // change 4 to 5 after auto payment
        }else if($status == 'canceled'){
            $orderStatus = '6';
        }else{
            $orderStatus = 'all';
        }

        echo "<prE>"; print_r($filter);  echo "</prE>"; die;

        if($filter['search'] != ''){  
            //DB::enableQueryLog();
            if($filter['start_date'] != '' && $filter['start_date'] != ''){   
                $orders = Order::whereHas('added_by_detail',function($query) use ($search){
                $query->  where('users.nick_name','LIKE', '%'.$search.'%');
                })->orwhereHas('supplier_detail',function($query) use ($search){
                    $query->  where('users.nick_name','LIKE', '%'.$search.'%');
                })
                ->orWhere(function($query) use ($search,$orderStatus,$start_date,$end_date){
                    $query->orWhere('total_item', 'LIKE', '%'.$search.'%');
                    $query->orWhere('total_qty', 'LIKE', '%'.$search.'%');
                    $query->orWhere('order_number', 'LIKE', '%'.$search.'%');
                    $query->orWhere('original_id', 'LIKE', '%'.$search.'%');
                    $query->whereBetween('created_at', [$start_date." 00:00:00",$end_date." 23:59:59"]);
                    if($orderStatus != 'all'){
                        $query->where('status',$orderStatus);   
                    }else{
                        $query->where('status','!=','7'); // Redirected order will not show to customer
                    }
                })
                ->where('added_by',Auth::user()->id)->orderBy('id','desc')->paginate($this->perPage); 

            }else{
                $orders = Order::whereHas('added_by_detail',function($query) use ($search){
                $query->  where('users.nick_name','LIKE', '%'.$search.'%');
                })->orwhereHas('supplier_detail',function($query) use ($search){
                    $query->  where('users.nick_name','LIKE', '%'.$search.'%');
                })
                ->orWhere(function($query) use ($search,$orderStatus){
                    $query->orWhere('total_item', 'LIKE', '%'.$search.'%');
                    $query->orWhere('total_qty', 'LIKE', '%'.$search.'%');
                    $query->orWhere('order_number', 'LIKE', '%'.$search.'%');
                    $query->orWhere('original_id', 'LIKE', '%'.$search.'%');
                    if($orderStatus != 'all'){
                        $query->where('status',$orderStatus);   
                    }else{
                        $query->where('status','!=','7'); // Redirected order will not show to customer
                    }
                })->where('added_by',Auth::user()->id)->orderBy('id','desc')->paginate($this->perPage);                 
            }
            //DB::enableQueryLog();            
        }else{   

            if($filter['start_date'] != '' && $filter['start_date'] != ''){               
                if($orderStatus != 'all'){
                    $orders = Order::with('supplier_detail','added_by_detail')->whereBetween('created_at', [$start_date." 00:00:00",$end_date." 23:59:59"])->where('status',$orderStatus)->where('added_by',Auth::user()->id)->orderBy('id','desc')->paginate($this->perPage);   
                }else{
                    $orders = Order::with('supplier_detail','added_by_detail')->whereBetween('created_at', [$start_date." 00:00:00",$end_date." 23:59:59"])->where('status','!=','7')->where('added_by',Auth::user()->id)->orderBy('id','desc')->paginate($this->perPage);    
                }

            }else{
                if($orderStatus != 'all'){
                    $orders = Order::with('supplier_detail','added_by_detail')->where('status',$orderStatus)->where('added_by',Auth::user()->id)->orderBy('id','desc')->paginate($this->perPage);   
                }else{
                    $orders = Order::with('supplier_detail','added_by_detail')->where('added_by',Auth::user()->id)->orderBy('id','desc')->paginate($this->perPage);    
                }                                          
            } 
        }
        $suppliers = User::whereHas('roles', function ($query) { $query->where('name', '=', 'supplier'); })->where('status','1')->orderBy('id','desc');
        $orderCOUNT = count($orders);
        return view($this->prefix.'/pagination',['filter'=> $filter,'value'=>$this->perPage,'orders'=>$orders,'OrderCount'=>$orderCOUNT,'url'=>$this->url]);
    }

    public function orderFilter(Request $request){
            
            $filter = \Crypt::decrypt($request->filter); 
            $filter = explode('/', $filter);

            $start_date = $filter[1];
            $end_date = $filter[2];

            if($filter[0] == 'pending'){
                $orders = Order::with('supplier_detail','added_by_detail')->where('added_by', Auth::user()->id)->whereBetween('created_at', [$start_date." 00:00:00",$end_date." 23:59:59"])->where('status','<',3)->orderBy('id','desc')->get();
            }elseif($filter[0] == 'dispatch'){
                $orders = Order::with('supplier_detail','added_by_detail')->whereBetween('created_at', [$start_date." 00:00:00",$end_date." 23:59:59"])->where('status','3')->orderBy('id','desc')->get();    
            }else{
                $orders = Order::with('supplier_detail','added_by_detail')->whereBetween('created_at', [$start_date." 00:00:00",$end_date." 23:59:59"])->orderBy('id','desc')->get();
            }
            $suppliers = User::whereHas('roles', function ($query) { $query->where('name', '=', 'supplier'); })->where('status','1')->orderBy('id','desc')->get();
            
            return view($this->prefix.'/order-filter',['title'=>'Orders','filter'=>$filter,'orders'=>$orders,'url'=>$this->url,'suppliers' => $suppliers]);
        }

    /**
     * Download All products.
     *
     * @return \Illuminate\Http\Response
     */
    public function downloadProducts(Request $request)
    {
        try
        {
            //echo Auth::user()->id;
            //$data = Product::with('product_price')->select('sku','product_name','brand_name','expiry_date','stock','id')->get();           
            $data = DB::table('products')
                ->leftJoin('product_prices', 'product_prices.product_id', '=', 'products.id')
                ->select('products.sku','products.product_name','products.brand_name','products.expiry_date','products.stock','products.id','product_prices.price')->where('product_prices.user_id',Auth::user()->id)
                ->get();     
            
            $data = json_decode(json_encode($data), true);         
            $fileName = "Product File(".date('Y-m-d').")";
            return Excel::create($fileName, function($excel) use ($data) {
                $excel->sheet('Products', function($sheet) use ($data)
                {
                    //$sheet->fromArray($data);
                    $sheet->fromArray($data,null,'A1',true);
                    $sheet->getColumnDimension('F')->setVisible(false);
                    $sheet->setColumnFormat([
                        'D' => 'yyyy-mm-dd',
                    ]);
                });
            })->download('xlsx');

        }
        catch (\Exception $e)
        {
            DB::rollback();
            $errors             = $e->getMessage();
            $res['success']     = false;
            $res['url']         = $this->url.'/product';
            $res['formErrors']  = true;
            $res['errors']      = $errors;
            echo json_encode($res);
            die();
        }  
    }

    /**
     * Read all apeals excel file and update order 
     *
     * @return \Illuminate\Http\Response
     */
    public function updateOldOrderId(Request $request)
    {
        //$orders = Order::where('added_by',Auth::user()->id)->get();
        $orderAppeal = OrderAppeal::get();
        //echo "<prE>"; print_r($orderAppeal->toArray()); die; 

        $fileNotExistArr = array();
        $fileExistArr = array();
        foreach ($orderAppeal as $key => $appeal) {
            if($appeal['id'] < 38) 
                continue;
            echo "</br>****************</br>";
            echo "</br>Appeal id =>".$appeal['id'];
            echo "</br>File path =>".$destinationPath = base_path().'/assets/orders/'.$appeal['file_name'];
            
            if (file_exists($destinationPath))  
            { 
                $fileExistArr[] = $appeal['id'];
                
                $data = \Excel::selectSheetsByIndex(0)->load($destinationPath)->get();
                $headerRow = $data->first()->keys()->toArray();
                $excelData = $data->toArray();
                //echo "<prE>"; print_r($excelData); die;

                $headerRow = $data->first()->keys()->toArray();
                $excelMapHistory = Excel_map_history::where('excel_filed',json_encode($headerRow))->first();
                //echo "<prE>"; print_r($excelMapHistory->toArray()); die;

                $mapfield = explode(',',$excelMapHistory->map_filed);
                //echo "<prE>"; print_r($mapfield); die;
                
                $originalIdField = str_replace('"','',$mapfield[11]);
                $originalIdArray = array();
                if($originalIdField != '' && $originalIdField != null){
                    foreach ($excelData as $key => $row) {
                        //echo $originalIdField."<prE>"; print_r($row); die;
                        if(!in_array($row[$originalIdField],$originalIdArray) && $row[$originalIdField] != ''){
                            $originalIdArray[] = $row[$originalIdField];
                        }
                    }
                }
                    
                // fetch apeal orders
                $orderData = Order::where('appeal_id',$appeal['id'])->get()->toArray();
                //echo "<prE>"; print_r($orderData); //die;   
                //echo "<prE>"; print_r($originalIdArray); die; 
                //if(count($orderData) == count($originalIdArray)){
                    foreach ($orderData as $key => $order) {
                        if(isset($originalIdArray[$key])){
                            $updateOrderData['original_id'] = $originalIdArray[$key];
                            $updateOrder = Order::updateOrCreate(['id' => $order['id']], $updateOrderData);     
                        }                        
                    }
                    //echo "Count same. updated";
                //}else{
                    //echo "Count not same. Not updated";
                //}
                                
            } 
            else 
            { 
                $fileNotExistArr[] = $appeal['id'];
            } 
                        
        }


        echo "<prE>"; print_r($fileExistArr);  
        echo "<prE>"; print_r($fileNotExistArr); die;   

        echo "<prE>"; print_r($orders->toArray()); die;
        
    }

    /**
     * Customer Balance.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function myBalance(Request $request)
    { 
        
        $filter['start_date'] =  $start_date = date('Y-m-d');
        $filter['end_date'] =  $end_date = date('Y-m-d');
       
        /*
        $u = User::Has('customer_order_info');
        $customers = $u->with(['customer_order_info'=>function($order_info) use($start_date,$end_date){           
                $order_info->whereBetween('created_at', [$start_date." 00:00:00",$end_date." 23:59:59"]);
                $order_info->whereIn('status',['0','1','2','3','4','5']);
                $order_info->select(DB::raw('sum(total_customer) as total_customer'),'added_by')->groupBy('added_by');
                //$order_info->sum('total_customer');
        }])->whereHas('roles', function ($i) use ($request) { return $i->where('role_id','3'); })->orderBy('id','desc')->paginate($this->perPage);
        */
        //echo "<pre>"; print_r($customers->toArray()); die;
        $customers = User::with(['customer_order_info'=>function($order_info) use($start_date,$end_date){           
            //$order_info->whereBetween('created_at', [$start_date." 00:00:00",$end_date." 23:59:59"]);
            $order_info->whereIn('status',['0','1','2','3','4','5']);
            $order_info->select(DB::raw('sum(total_customer) as total_customer'),'added_by')->groupBy('added_by');
            $order_info->sum('total_customer');
        }])->where('id',Auth::user()->id)->whereHas('roles', function ($i) use ($request) { return $i->where('role_id','3'); })->orderBy('id','desc')->paginate($this->perPage);    
        //echo "<pre>"; print_r($customers->toArray()); die;
        return view($this->prefix.'/balance',['filter'=> $filter,'customers'=>$customers,'value'=> $this->perPage,'title'=>'Customer Balance','url'=>$this->url]);
    }

    public function balanceDetail(Request $request)
    {  
        //dd($request); die('dead');
        $id        = $request->id;        
        $name      = $request->name;        
        $email     = $request->email;               
        $filter    = array();       

        if($request->start_date == ''){
            //$filter['start_date'] =  $start_date = date('Y-m-d');
            $filter['start_date'] =  $start_date = '';
        }else{
            $filter['start_date'] =  $start_date = $request->start_date;
        }
        
        if($request->end_date == ''){
            //$filter['end_date'] =  $end_date = date('Y-m-d');
            $filter['end_date'] =  $end_date = '';
        }else{
            $filter['end_date'] =  $end_date = $request->end_date;
        }
        
        if($filter['start_date'] != ''){
            //$orders = Order::where('added_by',$id)->whereBetween('created_at', [$start_date." 00:00:00",$end_date." 23:59:59"])->where('status','<','6')->get();
            $fullBalance = Order::select(DB::raw('sum(total_customer) as totalAmount'))->where('added_by',$id)->whereBetween('created_at', [$start_date." 00:00:00",$end_date." 23:59:59"])->where('status','<','6')->get();
        }else{
            //$orders = Order::where('added_by',$id)->where('status','<','6')->get();
            $fullBalance = Order::select(DB::raw('sum(total_customer) as totalAmount'))->where('added_by',$id)->where('status','<','6')->get();
        }

        $customers = User::with(['customer_order_info'=>function($coi) use($start_date,$end_date){
                $coi->whereBetween('created_at', [$start_date." 00:00:00",$end_date." 23:59:59"]);
                $coi->whereIn('status',['0','1','2','3','4','5']); 
            }])->whereHas('roles', function ($i) use ($request) { return $i->where('role_id','3'); })->orderBy('id','desc')->paginate($this->perPage);
    
        

        //$orders = Order::where('added_by',$id)->whereBetween('created_at', [$start_date." 00:00:00",$end_date." 23:59:59"])->where('status','<','6')->get();
        
        if($filter['start_date'] != ''){
            $o = OrderItems::whereHas('order_detail',function($od) use($start_date,$end_date,$id){
                $od->whereBetween('created_at', [$start_date." 00:00:00",$end_date." 23:59:59"]);
                $od->whereIn('status',['0','1','2','3','4','5']); 
                $od->where('added_by',$id);      
             });
            $orders = $o->with('order_detail')->get();    
        }else{
             $o = OrderItems::whereHas('order_detail',function($od) use($start_date,$end_date,$id){
                //$od->whereBetween('created_at', [$start_date." 00:00:00",$end_date." 23:59:59"]);
                $od->whereIn('status',['0','1','2','3','4','5']); 
                $od->where('added_by',$id);      
             });
            $orders = $o->with('order_detail')->get();    
        }
        
        //echo $id."<prE>"; print_r($orders->toArray()); echo "</pre>"; die; 
        $payments = Payments::select(DB::raw('sum(amount) as totalPayment'))->where('user_id',$id)->where('payment_status',1)->groupBy('user_id')->get();

        if(empty($payments->toArray())){
            $payments = 0;
        }else{
            $payments = $payments->toArray();
            $payments = $payments[0]['totalPayment'];
        }
        
        $balanceDetailsHtml = view($this->prefix.'/balance_details',['url'=>$this->url,'id'=>$id ,'name'=>$name, 'email'=>$email,'fullBalance'=>$fullBalance,'payments'=>$payments,'orders' => $orders,'supplier'=>$id,'title'=>'Customer Balance'])->render();
       
        $res['success']         = true;
        $res['delayTime']       = '5000';
        $res['html']            = $balanceDetailsHtml;
        echo json_encode($res);
    }

    /**
     * Get User Payments.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function getPayments(Request $request)
    {    
        // try
        // {            
            $id     = $request->id; 
            $email  = $request->email; 
            $name   = $request->name; 
            $payments = Payments::select(DB::raw('sum(amount) as totalPayment'))->where('user_id',$id)->where('payment_status',1)->groupBy('user_id')->get();
            $payments = $payments->toArray();
            //echo "<prE>"; print_r($payments); die;
            if(!empty($payments)){
                $res['success']         = true;
                $res['email']           = $email;
                $res['name']            = $name;
                $res['payment']         = $payments[0]['totalPayment'];               
                echo json_encode($res);
                die();
            }else{
                $res['success']         = true;
                $res['payment']         = 0;
                 echo json_encode($res);
                die();    
            }
            
        
        // catch (\Exception $e)
        // {
        //     DB::rollback();
        //     $errors             = $e->getMessage();            
        //     $res['success']     = false;
        //     $res['modalname']   = 'FullPayModal';
        //     $res['formErrors']  = true;
        //     $res['errors']      = $errors;
        //     echo json_encode($res);
        //     die();
        // }
    } 

    /**
     * Add Payments To User.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function Pay(Request $request)
    {                        
        
        try
        {            
            $id = $request->userid;
            $amount = $request->amount;
            $notes = $request->notes;

            $paymentdone = $request->paymentdone;
            $fullbalance = $request->fullbalance;

            $remaningAmt = $fullbalance - ($paymentdone+$amount);

            $remaningAmt = number_format($remaningAmt,2);

            $updatePayments = array();            
            $updatePayments['user_id'] = $id;
            $updatePayments['amount'] = $amount;
            $updatePayments['notes'] = $notes;
            $updateRes = Payments::updateOrCreate(['id'=>null],$updatePayments); 
            if($updateRes)
            {
                $res['success']         = true;
                $res['modalname']       = 'FullPayModal';
                $res['delayTime']       = '5000'; 
                $res['remaningAmt']     = $remaningAmt;
                $res['success_message'] = 'Payment added successfully';
                echo json_encode($res);
                die();
            }
        }
        catch (\Exception $e)
        {
            DB::rollback();
            $errors             = $e->getMessage();            
            $res['success']     = false;
            $res['modalname']   = 'FullPayModal';
            $res['formErrors']  = true;
            $res['errors']      = $errors;
            echo json_encode($res);
            die();
        }
    } 

    public function paymentOrder(Request $request)
    { 
        $date = date("Y-m-d");
         $chkpayments       =   DB::table('payments')
                ->where('payment_status', 2)
                ->where('created_at','!=', $date)                
            ->get();  
        //echo "<pre>"; print_r($chkpayments); echo "</pre>"; die();
        
        session_start();          
        $ordrNber               =   Helper::getLatestOrderid();
        $invoiceNumber          =   '';
        if(!empty($ordrNber))
        {
            $orderID            =   $ordrNber->crypto_payorderid;           
            if($orderID != "")
            {                     
              $dd               =   explode("-", $orderID);
              $lastNumber       =   ($dd[1] + 1);
              $invoiceNumber    =   ($dd[0].'-'.$lastNumber);
            }
            else
            {
              $invoiceNumber    =   'INV-1001';
            } 
        } 
        else
        {
            $invoiceNumber      =   'INV-1001';
        }    
        //echo "<pre>"     ; dd($request); echo "</pre>"; die();
        $fullbalance    =   $request->fullbalance;
        $username       =   $request->username;
        $usID           =   $request->userid;
        $useremail      =   $request->useremail;
        $paymentdone    =   $request->paymentdone;
        $userinfo       =   $request->username;
        $notesd         =   $request->notes; 
        $userid         =   $userinfo.'-'.$request->payment_website;
        $amount         =   $amountwithouttax =  $request->remaining; 
        $automatic      =   "automatic-insert";
        $payment_mode   =   $_SERVER['HTTP_USER_AGENT'];
        $date           =   date("Y-m-d H:i:s");

        $chkpayments       =   DB::table('payments')
                    ->where('user_id', $usID)
                    ->where('crypto_payorderid', $invoiceNumber)
                    ->Where('crypto_user_info', $userid)
                ->first();            
        if(!empty($chkpayments))
        {               
              
        }
        else
        {
             $data           =   array('user_id'=>$usID,"amount"=>$amountwithouttax,"payment_type"=>$automatic,"payment_mode"=>$payment_mode, "crypto_user_info"=>$userid,"crypto_payorderid"=>$invoiceNumber,"notes"=>$notesd,"created_at"=>$date,'payment_status'=>2);
            DB::table('payments')->insert($data); 
        }

        // $amount         =   $amount + ($amount / 100 * 2);         
        $baseUrl        =   url('/');
        $appPath        =   app_path();  
        $_SESSION['notesDataCore']  = $notesd; 
        $_SESSION['userOrgid']      = $request->userid; 
        $_SESSION['withouttaxAmt']  = $request->remaining;        
        DEFINE("CRYPTOBOX_PHP_FILES_PATH", $baseUrl ."/app/Services/lib/");         
        DEFINE("CRYPTOBOX_IMG_FILES_PATH", $baseUrl."/bitcoin_assets/images/");
        DEFINE("CRYPTOBOX_JS_FILES_PATH",   $baseUrl."/bitcoin_assets/js/");    
        DEFINE("CRYPTOBOX_LANGUAGE_HTMLID", "alang"); 
        DEFINE("CRYPTOBOX_COINS_HTMLID", "acoin");    
        DEFINE("CRYPTOBOX_PREFIX_HTMLID", "acrypto_");  
        $redirectUrl    =   $baseUrl.'/customer/my-balance';  
        $userID         =   $userid;        
        $userFormat     =   "COOKIE";     
        $orderID        =   $invoiceNumber;  
        $amountUSD      =   $amount;            
        $period         =   "NOEXPIRY";   // one time payment, not expiry
        $def_language   =   "en";     
        $def_coin       =   "speedcoin";   
        //$def_coin     =   "bitcoin";   
        $coins          =   array($def_coin);       
        $all_keys       =   array(  "speedcoin"   => array("public_key" => "46550AAYhSDaSpeedcoin77SPDPUBBAKf8mSJjmSRdiOkAxNLl",  
                            "private_key" => "46550AAYhSDaSpeedcoin77SPDPRVpbL55PbSYIHPuVWEgVCAy")             
                            );
       // $all_keys       =   array(  "bitcoin"   => array("public_key" => "47285AAr9nh6Bitcoin77BTCPUB0n5Kvu2JmMWKD4op1l1KvrE",  
       //                     "private_key" => "47285AAr9nh6Bitcoin77BTCPRVqtBz3jTjn2YAqxo14fBWQOe")             
       //                     );  
        $def_coin = strtolower($def_coin);
        if (!in_array($def_coin, $coins)) $coins[] = $def_coin;  
        foreach($coins as $v)
        {
            if (!isset($all_keys[$v]["public_key"]) || !isset($all_keys[$v]["private_key"])) die("Please add your public/private keys for '$v' in \$all_keys variable");
            elseif (!strpos($all_keys[$v]["public_key"], "PUB"))  die("Invalid public key for '$v' in \$all_keys variable");
            elseif (!strpos($all_keys[$v]["private_key"], "PRV")) die("Invalid private key for '$v' in \$all_keys variable");
            //elseif (strpos(CRYPTOBOX_PRIVATE_KEYS, $all_keys[$v]["private_key"]) === false) 
               // die("Please add your private key for '$v' in variable \$cryptobox_private_keys, file /lib/CryptoboxConfig.php.");
        }            
        $public_key  = $all_keys[$def_coin]["public_key"];
        $private_key = $all_keys[$def_coin]["private_key"];    
        $options = array(
          "public_key"    => $public_key,    
          "private_key"   => $private_key, 
          "webdev_key"    => "",          
          "orderID"       => $orderID,    
          "userID"        => $userID,   
          "userFormat"    => $userFormat,   // save userID in COOKIE, IPADDRESS, SESSION  or MANUAL
          "amount"        => 0,         // product price in btc/bch/bsv/ltc/doge/etc OR setup price in USD below         
          "amountUSD"     => $amountUSD,      // we use product price in USD        
          "period"        => $period,  
          "language"      => $def_language  
        );           
        $box              =     new CryptoboxClassTesT ($options);                  
        // Successful Cryptocoin Payment received
        if ($box->is_paid()) 
        {
            if (!$box->is_confirmed()) {
              $message =  "Thank you for order (order #".$orderID.", payment #".$box->payment_id()."). Awaiting transaction/payment confirmation";
            }                     
            else 
            { // payment confirmed (6+ confirmations)

              // one time action
              if (!$box->is_processed())
              {
                // One time action after payment has been made/confirmed
                // !!For update db records, please use function cryptobox_new_payment()!!
                 
                $message = "Thank you for order (order #".$orderID.", payment #".$box->payment_id()."). Payment Confirmed. <br>(User will see this message one time after payment has been made)"; 
                
                // Set Payment Status to Processed
                $box->set_status_processed();  
              }
                else $message = "Thank you for order (order #".$orderID.", payment #".$box->payment_id()."). Payment Confirmed. <br>(User will see this message during ".$period." period after payment has been made)"; // General message
            }
        }
        else $message       = "This order number has not been paid yet";         
        $pametBox           = view('bitcoin_payment/bitcoin-payment',['box'=>$box,'coins'=>$coins,'def_coin'=>$def_coin,'def_language'=>$def_language,'title'=>'Payment Order','amountwithouttax'=>$amountwithouttax, 'usID'=>$usID, 'amount'=>$amount,'message'=>$message,'redirect'=>$redirectUrl,'url'=>$this->url])->render();  
        return $pametBox; 
    }

    public function bitcoinpaymentOrderStatus(Request $request)
    { 
        $amount         =   $request->orginalAmut;
        $userOrgid      =   $request->usid;       
        $amountusd      =   $request->newData['amountusd'];
        $order          =   $request->newData['order'];
        $user           =   $request->newData['user'];
        $automatic      =   "automatic-ajax";
        $payment_mode   =   $_SERVER['HTTP_USER_AGENT'];
        $date           =   date("Y-m-d H:i:s");
        $aray           =   array('user_id'=>$userOrgid,"amount"=>$amount,"order"=>$order,"user"=>$user);
        $RequesData     =   serialize($aray);   
        $log_filename   =   "/var/www/html/dropshipper/app/Services/lib/PaymentStatus";
        if (!file_exists($log_filename))
        {         
            mkdir($log_filename, 0777, true);
        }
        $dd             =   $RequesData;       
        $log_file_data2 =   $log_filename.'/log_' .$userOrgid. date('d-M-Y H:i:s') . '.log';
        file_put_contents($log_file_data2, $dd . "\n", FILE_APPEND); 
        $output['user_id']          = $userOrgid;
        $output['amount']           = $amount;
        $output['success']          = true;
        $output['message']          = "success";
        echo json_encode($output);
    }

}
