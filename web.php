<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function ()
{
  if(Auth::check())
  {
    return redirect('/admin/');
  }
  else
  {
    return view('auth.login');
  }
});

/*Route::group(['middleware' => 'disablepreventback'],function()
{
    Auth::routes();
    Route::get('/home', 'HomeController@index');
})
*/

if(!function_exists('adminRoutes')) {
    function adminRoutes() {
        Route::group(['namespace' => 'Admin','middleware' => 'disablepreventback'], function()
  		{          
            Route::resource('/', 'DashboardController');
            Route::resource('dashboard', 'DashboardController');
            Route::resource('supplier', 'SupplierController');
            Route::resource('customer', 'CustomerController');
            Route::resource('employee', 'EmployeeController');
            Route::resource('product', 'ProductController');
            //Route::any('dashboard_filter', 'DashboardController@dateFilter');
            Route::get('dashboard_filter/{search}/{limit}/{orderStartDate}/{orderEndDate}/{orderStatus}/{orderAddedBy}', 'DashboardController@dateFilter');
            //Route::get('perpagecustomer/{value}/{search}','CustomerController@index');
            Route::post('getusermodal', 'DashboardController@getUserModal');
            Route::post('changeuserstatus', 'DashboardController@changeUserStatus');
            Route::post('changeproductStatus', 'DashboardController@changeproductStatus');
            Route::post('getempmodal', 'EmployeeController@getEmpModal');
            Route::post('getsupmodal', 'SupplierController@getSupModal');
            Route::post('getcusmodal', 'CustomerController@getCusModal');
            Route::post('getpromodal', 'ProductController@getProModal');
            Route::post('getprotypemodal', 'ProductController@getProTypeModal');
            Route::get('searchemployee/{search}/{limit}/{orderStartDate}/{orderEndDate}/{orderStatus}/{orderAddedBy}', 'EmployeeController@searchEmployee');
            Route::get('searchsupplier/{search}/{limit}/{orderStartDate}/{orderEndDate}/{orderStatus}/{orderAddedBy}', 'SupplierController@searchSupplier');
            Route::get('searchcustomer/{search}/{limit}/{orderStartDate}/{orderEndDate}/{orderStatus}/{orderAddedBy}', 'CustomerController@searchCustomer');
            Route::get('searchproduct/{search}/{limit}/{orderStartDate}/{orderEndDate}/{orderStatus}/{orderAddedBy}', 'ProductController@searchProduct');            
            Route::get('perpagecustomer/{value}/{search}','CustomerController@index');
            Route::get('perpagesupplie6r/{value}/{search}','SupplierController@index');
            Route::get('perpageemployee/{value}/{search}','EmployeeController@index');
            Route::get('perpageproduct/{value}/{search}','ProductController@index');
            
            Route::get('product_sku/{sku}','ProductController@productSku');
            Route::get('delete_product/{id}','ProductController@delete_product');
            Route::get('delete_supplier/{delete_id}','ProductController@delete_supplier');
            Route::resource('order-request', 'OrderAppealController');
            Route::post('request-detail', 'OrderAppealController@detailAppeal');
            Route::post('request-order-detail', 'OrderAppealController@detailAppealOrder');
            Route::post('assign-supplier', 'OrderAppealController@assignSupplier');
            Route::resource('order', 'OrderController');
            Route::post('order-detail', 'OrderController@detailOrder');
            // Closed
            Route::post('order_filter','OrderController@orderFilter');
            Route::get('order_request_filter/{filter}','OrderAppealController@orderRequestFilter');
            Route::get('perpageadmin_order/{value}/{search}','OrderController@index');
            Route::get('searchadmin_order/{search}/{limit}/{orderStartDate}/{orderEndDate}/{orderStatus}/{orderAddedBy}', 'OrderController@searchadmin_order');
            Route::get('perpageorder/{value}/{search}','OrderAppealController@index');
            Route::get('searchorder/{search}/{limit}/{orderStartDate}/{orderEndDate}/{orderStatus}/{orderAddedBy}','OrderAppealController@searchOrderAppeal');
            Route::post('delete-order-admin','OrderAppealController@deleteOrder');
            Route::any('customer-price-file/{id}','CustomerController@customerProductPriceFile');
            Route::post('update-customer-price','CustomerController@updateCustomerProductPrice');
            Route::any('supplier-price-file/{id}','SupplierController@customerSupplierPriceFile');
            Route::post('update-supplier-price','SupplierController@updateSupplierProductPrice');
            Route::post('upload-products','ProductController@uploadProducts');  
            Route::any('product-sample','ProductController@downloadProductSample'); 
            Route::any('products-download','ProductController@downloadProducts'); 
            Route::post('reject-order', 'OrderController@rejectOrder');
            Route::post('redirect-order', 'OrderController@redirectOrder');            
            Route::post('reship-order', 'OrderController@reshipOrder');
            Route::get('supplier-balance', 'SupplierController@supplierBalance'); 
            Route::get('customer-balance', 'CustomerController@customerBalance');  
            Route::post('supbalance-detail', 'SupplierController@balanceDetail'); 
            Route::post('cusbalance-detail', 'CustomerController@balanceDetail');  
            Route::any('pay', 'CustomerController@Pay'); 
            Route::post('payment-order', 'CustomerController@paymentOrder'); 
           
           // Route::get('payment-order/{userid}/{remaining}/{paymentdone}/{fullbalance}/{notes}', 'CustomerController@paymentOrder');
           
            Route::any('get-payments', 'CustomerController@getPayments');
            //Route::post('order-pay', 'SupplierController@orderPay');   
            Route::get('searchbalancesup/{search}/{limit}/{orderStartDate}/{orderEndDate}/{orderStatus}/{orderAddedBy}', 'SupplierController@searchBalance'); 
            Route::get('searchbalancecus/{search}/{limit}/{orderStartDate}/{orderEndDate}/{orderStatus}/{orderAddedBy}', 'CustomerController@searchBalance'); 

            Route::post('delete-customer', 'CustomerController@deleteCustomer');
            Route::post('delete-supplier', 'SupplierController@deleteSupplier');
            Route::any('delete-all-products','ProductController@deleteAllProducts'); 

            Route::any('edit-tracking-number','OrderController@updateTrackingNumber');
            Route::get('orders-download/{type}', 'OrderController@downloadOrder');
            Route::get('orders-statment/{type}', 'OrderController@downloadStatment');
            #Product Type 
            Route::any('product-type','ProductController@productType'); 
            Route::any('product-type-save','ProductController@productTypeSave'); 
            Route::any('product-type-edit/{id}','ProductController@productTypeEdit'); 
            Route::get('delete_product_type/{id}','ProductController@delete_productType');
            Route::post('delete-req-batch', 'OrderAppealController@deleteReqBatch');
            Route::post('get-redirect-model', 'OrderController@getRedirectModel');
              // Ghanshyam Edited
            Route::post('edit-orders', 'OrderController@update');
            Route::post('edit-orders/update', 'OrderController@updateorder');
            Route::get('ttp-download/{type}', 'DashboardController@downloadTTP');
            Route::get('label-script', 'LableScriptController@index');     
        });
  	}
}

if(!function_exists('supplierRoutes')) {
  function supplierRoutes() {
        Route::group(['namespace' => 'Supplier','middleware' => 'disablepreventback'], function()
        {
            Route::resource('/', 'DashboardController');
            Route::resource('/dashboard', 'DashboardController');
            Route::get('dashboard_filter/{search}/{limit}/{orderStartDate}/{orderEndDate}/{orderStatus}/{orderAddedBy}', 'DashboardController@dateFilter');
            Route::resource('order', 'OrderController');
            Route::post('order-detail', 'OrderController@detailOrder');
            Route::get('order_filter/{filter}','OrderController@orderFilter');
            //Route::post('order-tracking-id', 'OrderController@orderTrackingNumber');
            Route::get('order-download/{id}', 'OrderController@downloadOrder');
            Route::get('orders-download/{type}', 'OrderController@downloadAllOrder');
            Route::get('orders-statment/{type}', 'OrderController@downloadStatment');
            Route::get('orders-pending-download/{type}', 'OrderController@downloadPendingOrder');
            Route::post('order-change-status', 'OrderController@orderChangeStatus');
            Route::any('dashboard_filter', 'DashboardController@dateFilter');
            Route::post('assign-tracking', 'OrderController@assignTracking');
            Route::post('reject-order', 'OrderController@rejectOrder');
            Route::post('payment-received', 'OrderController@paymentReceiverOrder');
            Route::get('searchadmin_order/{search}/{limit}/{orderStartDate}/{orderEndDate}/{orderStatus}/{orderAddedBy}', 'OrderController@searchadmin_order');
            Route::get('perpageadmin_order/{value}/{search}','OrderController@index');
            Route::any('update-tracking-number', 'OrderController@updateTrackingNumber');
            Route::any('edit-tracking-number', 'OrderController@updateTrackingNumberManually');
        });
    }
}

if(!function_exists('customerRoutes')) {
  function customerRoutes() {
      Route::group(['namespace' => 'Customer','middleware' => 'disablepreventback'], function()
      {
            Route::resource('/', 'OrderController');
            Route::resource('dashboard', 'DashboardController');
            Route::get('dashboard_filter/{search}/{limit}/{orderStartDate}/{orderEndDate}/{orderStatus}/{orderAddedBy}', 'DashboardController@dateFilter');
            Route::resource('order', 'OrderController');
            Route::resource('accounting', 'OrderController');
            Route::post('order-detail', 'OrderController@detailOrder');
            Route::get('orders-download/{id}', 'OrderController@downloadOrder');
            Route::get('orders-statment/{id}', 'OrderController@downloadStatment');
            Route::any('order-export/{id}', 'OrderController@exportOrder');
            Route::get('order_filter/{filter}','OrderController@orderFilter');
            Route::resource('order-request', 'OrderAppealController');
            //Route::get('appeal-deleted-id', 'OrderAppealController@deletedAppealId');
            Route::any('dashboard_filter', 'DashboardController@dateFilter');
            Route::any('excelfield-mapping', 'OrderController@excelFieldMapping');
            Route::post('confirm-request', 'OrderController@confirmAppeal');
            Route::post('place-request', 'OrderController@placeAppeal');
            Route::post('request-detail', 'OrderController@detailAppeal');
            // Route::any('excelfield-mapping', 'OrderAppealController@excelFieldMapping');
            // Route::post('confirm-request', 'OrderAppealController@confirmAppeal');
            // Route::post('place-request', 'OrderAppealController@placeAppeal');
            // Route::post('request-detail', 'OrderAppealController@detailAppeal');
            // Route::get('perpageorder/{value}/{search}','OrderAppealController@index');
            // Route::get('searchorder/{search}/{limit}','OrderAppealController@searchOrder');
            Route::get('searchadmin_order/{search}/{limit}/{orderStartDate}/{orderEndDate}/{orderStatus}/{orderAddedBy}', 'OrderController@searchadmin_order');
            Route::get('searchadmin_accounting/{search}/{limit}/{orderStartDate}/{orderEndDate}/{orderStatus}/{orderAddedBy}', 'OrderController@searchadmin_order');
            Route::get('perpageadmin_order/{value}/{search}','OrderController@index');
            Route::get('perpageadmin_accounting/{value}/{search}','OrderController@index');
            Route::any('products-download','OrderController@downloadProducts');
            Route::any('update-old-orderid','OrderController@updateOldOrderId');
              // Ghanshyam Edited
            Route::post('edit-orders', 'OrderController@update');
            Route::post('edit-orders/update', 'OrderController@updateorder');
            
            // Customer My balance Routes
            Route::get('my-balance', 'OrderController@myBalance');
            Route::post('cusbalance-detail', 'OrderController@balanceDetail'); 
            Route::any('get-payments', 'OrderController@getPayments'); 
            Route::post('pay', 'OrderController@Pay'); 
            Route::post('payment-order', 'OrderController@paymentOrder'); 
            Route::post('bitcoin-payment-status', 'OrderController@bitcoinpaymentOrderStatus');             
      });
    }
}

Route::group(['prefix' => 'admin','middleware' => ['auth','role:admin']], function()
{
	adminRoutes();
});

Route::group(['prefix' => 'supplier','middleware' => ['auth','role:supplier','verified']], function()
{
	 supplierRoutes();
});

Route::group(['prefix' => 'customer','middleware' => ['auth','role:customer','verified']], function()
{
	 customerRoutes();
});

 

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', 'HomeController@logout')->name('logout');

Route::post('user/password/forget-password','Auth\ForgotPasswordController@postForgetPassword');
Route::get('new-password/{passwKey}', 'Auth\ForgotPasswordController@passwordView');
Route::post('resetPassword', 'Auth\ForgotPasswordController@resetPassword');
Route::post('updateProfile', 'ProfileController@updateProfile');

Route::any('cron-delete-bitcoinpayments', 'CronController@crondeletebitcoinpayments');

//Route::post('/user-login', 'Auth\LoginController@login');

#Login as user
//Route::get('connectAsUser/{id}', 'Auth\LoginController@loginAsUser');
Route::get('connectAsUser/{id}', 'ProfileController@loginAsUser');
Route::get('backToAdmin/{id}', 'ProfileController@loginAsAdmin');